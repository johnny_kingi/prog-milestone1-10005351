﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_7
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = 0;
            var counter = 12;
            var i = 0;

            Console.WriteLine("Please enter a number:  ");
            number = int.Parse(Console.ReadLine());


            for (i = 1; i <= counter; i++ )
            {
                Console.WriteLine($"{i} x {number} = {i*number}");
            }
        }
    }
}
