﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = new Dictionary<string, string>();
            days.Add("Monday", "weekday");
            days.Add("Tuesday", "weekday");
            days.Add("Wednesday", "weekday");
            days.Add("Thrusday", "weekday");
            days.Add("Friday", "weekday");
            days.Add("Saturday", "weekend");
            days.Add("Sunday", "weekend");

            foreach (var x in days)
            {
                if (x.Value == "weekend")

                {
                    Console.WriteLine($"{x.Key} is a weekend day");
                }
                
                else
                {
                    Console.WriteLine($"{x.Key} is a day of the week");
                }
            }

        }
    }
}
