﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            double input = 0;
            var choice = 0;


            Console.WriteLine("**** Welcome to temperature conversion calculator ****\n\n");

            Console.WriteLine("Enter 1 for Ceclius - Fahrenheit conversion or 2 for Fahrenheit - Cleclius conversion:  ");
            choice = int.Parse(Console.ReadLine());

            Console.WriteLine("Pleae enter number for conversion:  ");
            input = double.Parse(Console.ReadLine());

            if (choice == 1)
            {
                Console.WriteLine($"{input} degress Celcius = {input * 9 / 5 + 32} degrees Fahrenheit.");
            }

            else if (choice == 2)
            {
                Console.WriteLine($"{input} degress Fahrenheit = {(input - 32) * 5 / 9} degress Celcius.");
            }
            else
            {
                Console.WriteLine("Invalid Choice, run program again");
            }
                
        }
    }
}
