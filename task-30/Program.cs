﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input two numbers:  ");
            var x = Console.ReadLine();
            var y = Console.ReadLine();

            var a = 0;
            var b = 0;
            bool value = int.TryParse(x, out a);
            bool value_b = int.TryParse(y, out b);

            var z = x + y;
            Console.WriteLine($"the two numbers added as a string are: {z}\n");
            Console.WriteLine($"The two numbers added as a number are: {a + b}");

            Console.ReadLine();
        }
    }
}
