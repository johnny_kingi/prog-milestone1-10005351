﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter 3 words (Eg: I love bananas):  ");
            var input = Console.ReadLine();

            var output = input.Split(' ');

            var i = 0;

            do
            {                       
                Console.WriteLine($"{output[i]}");               
                i++;
            } while (i <= 2);

            Console.ReadLine();
        }
    }
}
