﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number to check if it can be devided by 3 or 4:  ");
            var a = int.Parse(Console.ReadLine());

            var b = a % 3;
            var c = a % 4;

            if (b == 0)
            {
                Console.WriteLine($"{a} is evenly devisible by 3, {a/3} times");

            }
            else if (c == 0)
            {
                Console.WriteLine($"{a} is evenly devisible by 4, {a/4} times");
            }
            else
            {
                Console.WriteLine($"{a} is not evenly devisible by 3 or 4");
            }
            Console.ReadLine();
        }
    }
}
