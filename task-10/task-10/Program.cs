﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_10
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = 2020;
            var i = 0;
            var years = new List<int> { };

            Console.WriteLine("**Welcome to leap year program**\n");

            for (i = 0; i < 20;)  //calculation loop to count leap years
            {
                i = i + 4;
                year = year + 4;
                years.Add (year);  // adding each leap year to list
            }
            
            Console.WriteLine($"There are {years.Count} leap years in the next 20 years");
            Console.ReadLine();

        }
    }
}
