﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_25
{
    class Program
    {
        static void Main(string[] args)
        {          
                Console.WriteLine("Please type in a number and press ENTER:  ");
                var number = Console.ReadLine();

                var a = 0;
                bool value = int.TryParse(number, out a);              
                
            if (value == false)
            {
                Console.WriteLine($"You typed in {number}");
                Console.WriteLine("You did not enter a NUMBER please start again");
            }
            else
            {
                Console.WriteLine($"Congratulations you enterd a number:  {number}");
            }
            Console.ReadLine();
        }
    }
}
