﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_34
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in the number of weeks and press ENTER:  ");
            var weeks = int.Parse(Console.ReadLine());

            var tot_days = weeks * 7;
            var weekends = weeks * 2;

            Console.WriteLine($"there are {tot_days - weekends} working days in {weeks} weeks");

        }
    }
}
