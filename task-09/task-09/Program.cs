﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = 2020;
            var i = 0;

            Console.WriteLine("**Welcome to leap year program**\n");
            Console.WriteLine("2016 is a leap year");

            for (i = 0; i < 20;)
            {
                i = i + 4;                
                Console.WriteLine($"Then {year}");
                year = year + 4;
            }

        }
    }
}
