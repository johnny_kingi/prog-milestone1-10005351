﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_16
{
    class Program
    {
        static void Main(string[] args)
        {
            var word = "hello";

            Console.WriteLine("Please type a word");
            word = (Console.ReadLine());
            Console.WriteLine($"{word} has {word.Length} letters");
        }
    }
}
