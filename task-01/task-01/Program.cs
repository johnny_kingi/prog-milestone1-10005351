﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "Johnny";
            var age = 31;
            

            Console.WriteLine("Hello, Please enter your name:  ");
            name = Console.ReadLine();
            Console.WriteLine($"Thanks {name}.  Now please enter your age:  "); //WriteLine method 1//
            age = int.Parse(Console.ReadLine());

            Console.WriteLine("Thanks {0} ",name); //WriteLine method 2//

            Console.WriteLine(name + " you are " + age + " years old"); //WriteLine Method 3


        }
    }
}
