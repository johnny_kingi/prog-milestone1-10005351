﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = 0;           

            Console.WriteLine("***Welcome to odds & evens verifier program***\n");

            Console.WriteLine("Please choose a number:  ");
            number = int.Parse(Console.ReadLine());

            var result = number % 2;           

            if (result == 0)
            {
                Console.WriteLine("This is an even number");
            }
            else
            {
                Console.WriteLine("You have chosen an odd number");
            }

        }
    }
}
