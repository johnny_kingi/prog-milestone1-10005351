﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            var hours = 0;
            int minutes = 0;

            Console.WriteLine("*** Welcome to 24hr clock converter ***\n");
            Console.WriteLine("Pleae enter the first two numbers of your 24hr number:  ");
            hours = int.Parse(Console.ReadLine());
            Console.WriteLine("Please enter the last two numbers of your 24 hr time:  ");
            minutes = int.Parse(Console.ReadLine());

            if (hours > 13 &&  hours < 24 )

            {                
                Console.WriteLine($"The time is {hours - 12}.{minutes} pm");
            }

            else if ( hours < 12 )
            {
                Console.WriteLine($"The time is {hours}.{minutes} am");
            }
            else if ( hours == 12 )
            {
                Console.WriteLine($"The time is {hours}.{minutes} pm");
            }           
        }
    }
}
