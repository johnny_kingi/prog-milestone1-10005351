﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var months = new Dictionary<string, int>();
            var months_31 = new List<string> { };

            months.Add("January", 31);
            months.Add("February", 28);
            months.Add("March", 31);
            months.Add("April", 30);
            months.Add("May", 31);
            months.Add("June", 30);
            months.Add("July", 31);
            months.Add("August", 31);
            months.Add("September", 30);
            months.Add("October", 31);
            months.Add("November", 30);
            months.Add("December", 31);


            foreach (var x in months)
            {

                if (x.Value == 31)
                {
                    months_31.Add(x.Key);
                }

            }

            Console.WriteLine($"There is a total of {months_31.Count} months with 31 days, They are:\n");
            months_31.ForEach(Console.WriteLine);
            Console.ReadLine();
        }
    }
}
