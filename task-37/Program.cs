﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            const int credits = 15;
            const int hrs_per_crdt = 10;
            const int semester = 12;
            const int lect_time = 5;

            var hrs_to_study = credits * hrs_per_crdt - (semester * lect_time);

            Console.WriteLine($"{hrs_to_study / semester} hours per week should be used to study per 15 credit paper");

        }
    }
}
