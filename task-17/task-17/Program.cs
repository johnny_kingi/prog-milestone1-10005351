﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, int>>();

            names.Add(Tuple.Create("Johnny", 31));
            names.Add(Tuple.Create("Sarah", 18));
            names.Add(Tuple.Create("Passion", 69));

            foreach (var x in names)
            {
                Console.WriteLine($"{x.Item1} , {x.Item2}");           
            }

        }
    }
}
